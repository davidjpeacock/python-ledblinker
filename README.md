# LEDBlinker

This is an example Python program which blinks an LED on a breadboard attached 
to a Raspberry Pi.

## Components

* One of the following: Raspberry Pi B, Raspberry Pi 2, or Raspberry Pi 3
* One breadboard
* One LED
* One 330Ω resistor
* Two male-female jumper wires

## Wiring Diagram

![Fritzing Raspberry Pi + Breadboard Diagram](LEDBlinker_bb.png)

## Usage

* `chmod +x ledblinker.py`
* `./ledblinker.py`
